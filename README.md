<!--
SPDX-FileCopyrightText: 2020 - 2023 Robin Vobruba <hoijui.quaero@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

# TsDC tools

[![License: GPL v3](
    https://img.shields.io/badge/License-GPLv3-blue.svg)](
    https://www.gnu.org/licenses/gpl-3.0)
[![REUSE status](
    https://api.reuse.software/badge/gitlab.com/OSEGermany/oh-tsdc-tools)](
    https://api.reuse.software/info/gitlab.com/OSEGermany/oh-tsdc-tools)

Simple script-like tools to convert different formats of TsDC into each other.
This software is written in Kotlin, which runs on the Java Virtual Machine (JVM).

> NOTE:\
> You may want to use the
> [**latest build artifacts**](https://osegermany.gitlab.io/oh-tsdc-tools/)
> in your own CI builds,\
> so you do not have to compile them each time from scratch.

## Compiling

You need [Maven 3](https://maven.apache.org/install.html) installed on your system.

On linux, this is usually the `maven` package,
so on Ubuntu or Debian you can install it with:

```bash
apt-get install maven
```

Then compile with:

```bash
mvn package
```

## Conversions

### From RDF to structured Markdown

Converting the TsDC Standard into a Markdown structure
(for more accessible inspection of the class-structure).

out there:

```bash
project_dir="$(pwd)/../oh-tsdc"
java -classpath target/*-jar-with-dependencies.jar \
    de.opensourceecology.tsdc.tools.rdf2md.MainKt \
    --input "$project_dir/tsdc.ttl" \
    --structure-property \
        "https://w3id.org/oseg/ont/tsdc/core#isBelow" \
    --output "$project_dir/tsdc.md"
```

You should now have the Markdown file at _$project_dir/tsdc.md_.

### From structured Markdown to RDF

This imports the hand-crafted, temporary,
deprecated Markdown TSDC DB _TsDC-DB.md_ into RDF.

out there:

```bash
project_dir="$(pwd)/../oh-tsdc"
java -classpath target/*-jar-with-dependencies.jar \
    de.opensourceecology.tsdc.tools.md2rdf.MainKt \
    --md-in "$project_dir/TsDC-DB.md" \
    --rdf-in "$project_dir/tsdc-req.ttl" \
    --rdf-out "$project_dir/tsdc-req_GENERATED.ttl"
```

You should now have the combined, generated RDF file at _$project_dir/tsdc-req_GENERATED.ttl_.

### Reformatting (from RDF to RDF)

This reformats an arbitrary RDF file
and outputs a sorted and formatted RDF/Turtle file.

out there:

```bash
project_dir="$(pwd)/../oh-tsdc"
java -classpath target/*-jar-with-dependencies.jar \
    de.opensourceecology.tsdc.tools.rdf2rdf.MainKt \
    --input "$project_dir/tsdc.ttl" \
    --output "$project_dir/tsdc_REFORMATTED.ttl"
```

You should now have the reformatted RDF/Turtle file at _$project_dir/tsdc_REFORMATTED.ttl_.
