// SPDX-FileCopyrightText: 2020 Robin Vobruba <hoijui.quaero@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package de.opensourceecology.tsdc.tools.rdf2md

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import java.io.File

class ParsedArgs(parser: ArgParser) {

    val rdfInputFile by parser.storing(
        "-i", "--input",
        help = "RDF input file path") { File(this) }
        .default<File?>(null)

    val outputMd by parser.storing(
        "-o", "--output",
        help = "path to the Markdown output file") { File(this) }
        .default<File?>(null)

    val structureProperty by parser.storing(
        "-p", "--structure-property",
        help = "IRI of the RDF property (sub-property of rdfs:subClassOf) used to build the document structure")
        .default("http://www.w3.org/2000/01/rdf-schema#subClassOf")
}
