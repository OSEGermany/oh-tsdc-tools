// SPDX-FileCopyrightText: 2020 Robin Vobruba <hoijui.quaero@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package de.opensourceecology.tsdc.tools.rdf2md

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.mainBody
import org.apache.jena.graph.NodeFactory
import org.apache.jena.ontology.impl.ObjectPropertyImpl
import org.apache.jena.rdf.model.*
import java.io.File
import java.io.PrintStream
import java.util.*
import kotlin.collections.HashMap

/**
 * Reads a structured (TsDC) RDF (using tsdc:isBelow for structure inference),
 * and displays it as (structured) Markdown.
 */
fun main(args: Array<String>) : Unit = mainBody {

    ArgParser(args).parseInto(::ParsedArgs).run {

        checkNotNull(rdfInputFile) { "Please provide an RDF input file with -i or --input" }
        checkNotNull(outputMd) { "Please provide the path to the Markdown output file with -o or --output" }

        println("Converting RDF file '${rdfInputFile!!.absolutePath}'" +
                " to Markdown ('${outputMd!!.absolutePath}')" +
                " using RDF property '$structureProperty' for the structuring ...")

        // read RDF
        val model = ModelFactory.createDefaultModel()
        model.read(rdfInputFile!!.toURI().toString(), "TURTLE")

        // parse RDF model into (our internal) node objects
        val nodes = HashMap<String, NodeObj>()
        val nStr : String? = null
        for (statement in model.listStatements(null, ObjectPropertyImpl(NodeFactory.createURI(
            structureProperty), null), nStr))
        {
            val sUri = statement.subject.uri
            val sNode = nodes.getOrPut(sUri, { NodeObj(sUri) })

            val oUri = statement.`object`.toString()
            val oNode = nodes.getOrPut(oUri, { NodeObj(oUri) })

            sNode.above = oNode
            oNode.below.add(sNode)
        }
        val topNode = addTopNode(nodes)
        sortBelow(nodes)

        // write the structured Markdown
        writeMarkdown(outputMd!!, model, topNode)
    }
}

fun writeMarkdown(outputMd: File, model: Model, topNode: NodeObj) {

    val outputMdStream = PrintStream(outputMd.outputStream())
    var topIndex = 1
    topNode.below.forEach { writeNodeMarkdown(outputMdStream, model, it, 1, "${topIndex++}.") }
}

fun writeNodeMarkdown(outputMdStream: PrintStream, model: Model, node: NodeObj, level: Int, index: String) {

    // print our header line
    outputMdStream.println("#".repeat(level) + " [${index} " + node.toString() + "](" + node.uri + ")")
    outputMdStream.println()

    // print all our properties
    val resource = model.getResource(node.uri)
    resource.listProperties().forEach { prop ->
        val predicateMd = "[**${prop.predicate.localName}**](${prop.predicate.uri})"
        val objectMd = if (prop.`object`.isLiteral) {
            val objDtUri = prop.`object`.asLiteral().datatypeURI
            if (objDtUri.endsWith("XMLSchema#string") or objDtUri.endsWith("rdf-syntax-ns#langString")) {
                "\"${prop.`object`.asLiteral().lexicalForm}\""
            } else {
                prop.`object`.asLiteral().lexicalForm
            }
        } else {
            "[" + prop.`object`.asResource().localName + "](${prop.`object`.asResource().uri})"
        }
        outputMdStream.println("* $predicateMd $objectMd")
    }
    outputMdStream.println()

    // print our below classes
    var belowIndex = 1
    node.below.forEach { writeNodeMarkdown(outputMdStream, model, it, level + 1, "$index${belowIndex++}.") }
}

fun sortBelow(nodes: Map<String, NodeObj>) {

    nodes.forEach { node -> node.value.below.sortBy { belowNode -> belowNode.uri } }
}

fun addTopNode(nodes: Map<String, NodeObj>) : NodeObj {

    val topNode = NodeObj("[TOP]")
    nodes.forEach {
        if (it.value.above == null) {
            it.value.above = topNode
            topNode.below.add(it.value)
        }
    }
    return topNode
}

fun extractName(uri: String) : String {
    return uri.substringAfterLast('#')
}

class NodeObj(val uri: String, var above: NodeObj? = null, val below: MutableList<NodeObj> = LinkedList()) {

    override fun toString() : String {
        return extractName(uri)
    }
}
