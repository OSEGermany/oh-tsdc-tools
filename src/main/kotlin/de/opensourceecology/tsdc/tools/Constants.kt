// SPDX-FileCopyrightText: 2020 - 2023 Robin Vobruba <hoijui.quaero@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package de.opensourceecology.tsdc.tools

const val IRI_TSDC : String = "https://w3id.org/oseg/ont/tsdc/core#"
const val IRI_TSDC_REQ : String = "https://w3id.org/oseg/ont/tsdc/requirements#"
const val IRI_OWL : String = "http://www.w3.org/2002/07/owl#"
const val IRI_RDF : String = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
const val IRI_RDFS : String = "http://www.w3.org/2000/01/rdf-schema#"
