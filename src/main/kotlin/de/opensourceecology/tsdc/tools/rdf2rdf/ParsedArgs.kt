// SPDX-FileCopyrightText: 2020 Robin Vobruba <hoijui.quaero@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package de.opensourceecology.tsdc.tools.rdf2rdf

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import java.io.File

class ParsedArgs(parser: ArgParser) {

    val inputFile by parser.storing(
        "-i", "--input",
        help = "path to the RDF input file") { File(this) }
        .default<File?>(null)

    val outputFile by parser.storing(
        "-o", "--output",
        help = "path to the RDF output file") { File(this) }
        .default<File?>(null)

    val indent by parser.storing(
        "--indent",
        help = "This string is used as a single indent")
        .default("  ")

    val endDotOnNewLine by parser.flagging(
        "-d", "--dot",
        help = "To put the subject ending '.' on a new line")

    private val nonTypedLiterals by parser.flagging(
        "--non-typed",
        help = "Do not include type information for literals " +
                "(\"1\"^^xsd:integer -> 1)")

    val passThrough by parser.flagging(
        "-p", "--pass-through",
        help = "Do a Jena parsing and (re-)serializing, without any other formatting")

    val prefixAligning by parser.flagging(
        "--align-prefixes",
        help = "Align all the prefix URIs")

    val typedLiterals = !nonTypedLiterals
}
