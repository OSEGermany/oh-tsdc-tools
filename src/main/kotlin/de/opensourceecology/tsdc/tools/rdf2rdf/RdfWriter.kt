// SPDX-FileCopyrightText: 2020-2024 Robin Vobruba <hoijui.quaero@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package de.opensourceecology.tsdc.tools.rdf2rdf

import de.opensourceecology.tsdc.tools.*
import org.apache.jena.rdf.model.*
import java.io.File
import java.io.PrintWriter
import kotlin.math.max

private val NULL_STRING: String? = null

/**
 * Takes an RDF model and formats it into a Turtle formatted string,
 * so it becomes easily comparable.
 */
class RdfWriter(private val model: Model, private val args: ParsedArgs) {

    private val propA : Property = model.createProperty("${IRI_RDF}type")
    private val resOntology : Resource = model.createResource("${IRI_OWL}Ontology")
    private val base = extractBaseUri(args.inputFile!!)

    companion object {
        fun extractBaseUri(rdfFile: File): String? {
            // print the base
            var base: String? = null
            rdfFile.forEachLine { line ->
                if (line.trim().startsWith("@base")) {
                    base = "^@base.*<(.*)>.*[.]$"
                        .toRegex()
                        .find(line.trim())!!
                        .groupValues[1]
                }
            }
            return base
        }
    }

    private fun makeRelative(uri: String, addBracketsIfRelative: Boolean = true) : String {

        return if (base != null && uri.startsWith(base)) {
            val newUri = uri.substring(base.length)
            if (addBracketsIfRelative) {
                "<$newUri>"
            } else {
                newUri
            }
        } else {
            uri
        }
    }

    fun writeTo(out: PrintWriter) {

        val prefixes = sortedPrefixes()
        // prepare for base and prefix alignment
        var maxPrefixLen = 0
        if (args.prefixAligning) {
            prefixes.forEach { maxPrefixLen = max(maxPrefixLen, it.key.length) }
        }

        // print the base
        if (base != null) {
            val filler =
                if (args.prefixAligning) " ".repeat(maxPrefixLen + 4)
                else ""
            out.println("@base $filler<$base> .")
        }

        // print the prefixes
        for ((prefix, uri) in prefixes) {
            val filler = " ".repeat(maxPrefixLen - prefix.length)
            val cleanedUri = makeRelative(uri, false)
            out.println("@prefix ${prefix}: ${filler}<${cleanedUri}> .")
        }

        // print the statements
        val subjects = sortedSubjects()
        for (subject in subjects) {
            out.println()
            out.println(subjectToString(subject))

            val subjStatements = statementsFor(subject)
            var prev = false
            for ((predicate, obj) in subjStatements) {
                if (prev) {
                    out.println(" ;")
                }
                out.print("${args.indent}${predicate} $obj")
                prev = true
            }
            if (prev) {
                if (args.endDotOnNewLine) {
                    out.println(" ;")
                    out.println("${args.indent}.")
                } else {
                    out.println(" .")
                }
            }
        }
    }

    private fun sortedPrefixes() : Map<String, String> {

        return model
            .nsPrefixMap
            .toSortedMap()
    }

    private fun subjectToString(subject: Resource) : String {
        return makeRelative(model.shortForm(subject.uri))
    }

    private fun predicateToString(predicate: Resource) : String {
        return makeRelative(model.shortForm(predicate.uri))
    }

    private fun sortedSubjects() : List<Resource> {

        return model
            .listSubjects()
            .toList()
            .sortedBy { subj ->
                if (isOntology(subj)) {
                    // ontology first!
                    ""
                } else {
                    subj.localName
                }
            }
    }

    /**
     * Creates the list of (predicate, object) pairs associated with the given subject.
     * The values are already (mostly) ready for printout.
     */
    private fun statementsFor(subject: Resource) : List<Pair<String, String>> {

        return model
            .listStatements(subject, null, NULL_STRING)
            .toList()
            .map { statement ->
                var predicate = predicateToString(statement.predicate)
                if (predicate == "rdf:type") {
                    predicate = "a"
                }
                val obj = objectToString(statement.`object`)
                Pair(predicate, obj)
            }
            .sortedWith(compareBy({
                when (it.first) {
                    "a" -> "" // make this always appear first
                    "tsdc:isBelow" -> " " // HACK a TsDC specific thing! - make this always appear right-after the above
                    else -> it.first
                }
            }, {
                // if the above (key) is same, sort further by this:
                it.second
            }))
    }

    private fun isOntology(subj: Resource?): Boolean {
        return model.listStatements(subj, propA, resOntology).hasNext()
    }

    /**
     * Answer a string describing <code>object</code>, quoting it if it is a literal.
     */
    private fun objectToString(obj: RDFNode) : String {
        return if (obj.isURIResource) {
            makeRelative(model.shortForm(obj.asResource().uri))
        } else {
            // we have a literal
            val value = obj.asLiteral().value
            // only quote strings
            if (value is String) {
                // use ''' quotes for multi-line strings
                if (value.contains('\n')) {
                    "'''$value'''"
                } else {
                    "\"$value\""
                }
            } else {
                if (args.typedLiterals) {
                    var objStr = obj.asNode().toString(model)
                    if (objStr.contains("^^")) {
                        val (_, content, typeUri) =
                            "^(.*)\\^\\^(.*)$".toRegex().find(objStr)!!.groupValues
                        objStr = "${content}^^${model.shortForm(typeUri)}"
                    }
                    objStr
                } else {
                    value.toString()
                }
            }
        }
    }
}
