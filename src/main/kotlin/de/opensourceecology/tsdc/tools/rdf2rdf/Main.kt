// SPDX-FileCopyrightText: 2020 Robin Vobruba <hoijui.quaero@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package de.opensourceecology.tsdc.tools.rdf2rdf

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.mainBody
import org.apache.jena.rdf.model.ModelFactory

/**
 * Reads an RDF file and reformats it, so it becomes easily comparable.
 */
fun main(args: Array<String>) : Unit = mainBody {

    ArgParser(args).parseInto(::ParsedArgs).run {

        checkNotNull(inputFile) { "Please provide an RDF input file with -i or --input" }
        checkNotNull(outputFile) { "Please provide the path to the RDF output file with -o or --output" }

        println("Converting RDF file '${inputFile!!.absolutePath}'" +
                " to RDF ('${outputFile!!.absolutePath}'), reformatting ...")

        // read RDF
        val model = ModelFactory.createDefaultModel()
        model.read(inputFile!!.toURI().toString(), "TURTLE") // TODO should we remove the lang here, to support more RDF languages?

        // write RDF/Turtle
        if (passThrough) {
            val base = RdfWriter.extractBaseUri(outputFile!!)
            model.write(outputFile!!.outputStream(), "TURTLE", base)
        } else {
            val rdfWriter = RdfWriter(model, this)
            outputFile!!.printWriter().use { out ->
                rdfWriter.writeTo(out)
            }
        }
    }
}
