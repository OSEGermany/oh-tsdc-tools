// SPDX-FileCopyrightText: 2020 Robin Vobruba <hoijui.quaero@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package de.opensourceecology.tsdc.tools.md2rdf

import de.opensourceecology.tsdc.tools.*
import org.apache.jena.rdf.model.Model

class PropertyHP(
    args: ParsedArgs,
    origName: String,
    parent: HierarchyPart?,
    private var requiredInfo: Boolean,
    onlyIfTechnicallyNecessary: Boolean = true,
    children: MutableList<HierarchyPart> = mutableListOf())
    : HierarchyPart(args, origName, parent, onlyIfTechnicallyNecessary, children)
{
    override fun addTo(model: Model) {

        val itemName = origName
        val itemPart = MdPart.fromName(itemName)
        val onlyIfTechnicallyNecessary = onlyIfTechnicallyNecessary

        // Create the property (if non-existent)
        val resEntity = model.createResource("${IRI_TSDC_REQ}${getMyName()}")
        val resParentEntity = model.createResource("${IRI_TSDC_REQ}${getParentName()}")
        val propA = model.createProperty("${IRI_RDF}type")
        val propIsBelow = model.createProperty( "${IRI_TSDC}isBelow")
        val propComment = model.createProperty("${IRI_RDFS}comment")
        val resClass = model.createResource("${IRI_OWL}Class")
        val entityIsClass = model.createStatement(resEntity, propA, resClass)
        val resReqInfo = model.createResource("${IRI_TSDC_REQ}RequiredInformation")
        val resCommonSrc = model.createResource("${IRI_TSDC_REQ}CommonSource")
        if (!model.contains(entityIsClass)) {
            // this entity is not yet in the input RDF, so we write it
            model.add(entityIsClass)

            val superCls = if (requiredInfo) {
                resReqInfo
            } else {
                resCommonSrc
            }
            model.add(model.createStatement(resEntity, propIsBelow, superCls))

            if (itemPart.comment != null) {
                model.add(model.createStatement(resEntity, propComment, itemPart.comment))
            }
        }

        // register the property with the parents class
        if (isLeaf()) {
            val propReqInfo = model.createProperty("${IRI_TSDC_REQ}requiredInfo")
            val propReqInfoITN = model.createProperty("${IRI_TSDC_REQ}requiredInfoIfTechnicallyNecessary")
            val propComSrcFile = model.createProperty("${IRI_TSDC_REQ}commonSourceFiles")
            val propComSrcFileITN = model.createProperty("${IRI_TSDC_REQ}commonSourceFilesIfTechnicallyNecessary")
            val propertyProp = if (requiredInfo) {
                if (onlyIfTechnicallyNecessary) {
                    propReqInfoITN
                } else {
                    propReqInfo
                }
            } else {
                if (onlyIfTechnicallyNecessary) {
                    propComSrcFileITN
                } else {
                    propComSrcFile
                }
            }
            model.add(model.createStatement(resParentEntity, propertyProp, resEntity))
        }
    }
}
