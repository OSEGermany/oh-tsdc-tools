// SPDX-FileCopyrightText: 2020 Robin Vobruba <hoijui.quaero@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package de.opensourceecology.tsdc.tools.md2rdf

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.mainBody
import de.opensourceecology.tsdc.tools.rdf2rdf.RdfWriter
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import java.io.File
import java.lang.IllegalStateException

/**
 * Read a structured Markdown file
 * and a preliminary TsDC-requirements RDF,
 * extend the later with the info from the former
 * and write the result to an new RDF file.
 */
fun main(args: Array<String>) : Unit = mainBody {

    ArgParser(args).parseInto(::ParsedArgs).run {

        println("Converting structured Markdown DB '${inputMd.absolutePath}' " +
                "to RDF '${outputRdf.absolutePath}' ...")

        // extract structure/hierarchy from the Markdown file
        val inputMdRoot = extractHierarchy(this, inputMd)

        // write the RDF/Turtle result
        convert(inputMdRoot, inputRdf, outputRdf)
    }
}

enum class State {
    OUTSIDE,
    INSIDE,
    REQ_INFO,
    COMMON_SOURCES;
}

fun extractHierarchy(args: ParsedArgs, inputMd: File) : HierarchyPart {

    println("Parsing '${inputMd.absolutePath}' into a tree structure.")

    val root = ClassHP(args, "", null)
    var directPredecessor : HierarchyPart = root
    var propPredecessor : HierarchyPart? = null

    var state = State.OUTSIDE
    var parts = mutableListOf<MdPart>()
    var level = 0
    inputMd.readLines().forEach { line ->
        when {
            line.isEmpty() -> {
                // do nothing
                return@forEach
            }
            line == "required information:" -> {
                state = State.REQ_INFO
            }
            line == "common source:" -> {
                state = State.COMMON_SOURCES
            }
            line.trim().startsWith("- ") -> {
                if ((state != State.REQ_INFO) and (state != State.COMMON_SOURCES)) {
                    throw IllegalStateException()
                }
                val itemNameRaw = "^- (.+)$".toRegex().find(line.trim())!!.groupValues[1]
                val propLevel = "^[ ]*".toRegex().find(line)!!.value.length / 2
                val itemPart = MdPart.fromName(itemNameRaw)
                val itemParts = listOf(itemPart) + parts
                val onlyIfTechnicallyNecessary = itemParts.fold(false) {
                        cur: Boolean, mdPart: MdPart ->
                    cur or mdPart.onlyIfTechnicallyNecessary
                }
                val requiredInfo = (state == State.REQ_INFO)

                val predecessor =
                    if (propLevel == 0) directPredecessor
                    else propPredecessor!!
                propPredecessor = PropertyHP(
                    args,
                    itemPart.name,
                    predecessor,
                    requiredInfo,
                    onlyIfTechnicallyNecessary)
            }
            line.startsWith("##") -> {
                state = State.INSIDE
                val newLevel = "^#+".toRegex().find(line)!!.value.length - 1
                val partNameRaw = "# (.+)$".toRegex().find(line)!!.groupValues[1]
                val part = MdPart.fromName(partNameRaw)
                parts = parts.subList(0, newLevel - 1)
                parts.add(part)

                for (step in level downTo newLevel) {
                    directPredecessor = directPredecessor.parent!!
                }

                directPredecessor = ClassHP(args, part.name, directPredecessor)
                level = newLevel
            }
            else -> {
                println("Ignored line: $line")
            }
        }
    }

    return root
}

fun convertPart(mdInputPart: HierarchyPart, model: Model) {

    mdInputPart.children.forEach { child ->
        child.addTo(model)
        if (!child.isLeaf()) {
            convertPart(child, model)
        }
    }
}

fun convert(mdInputRoot: HierarchyPart, inputRdf: File, outputRdf: File) {

    println("Writing Markdown info, combining it with '${inputRdf.absolutePath}' " +
            "into new file '${outputRdf.absolutePath}'.")

    val model = ModelFactory.createDefaultModel()
    model.read(inputRdf.toURI().toString(), "TURTLE")
    val base = RdfWriter.extractBaseUri(inputRdf)

    mdInputRoot.children.forEach { child ->
        convertPart(child, model)
    }

    val outputRdfWriter = outputRdf.writer()
    // HACK We need ot do this manually here,
    // because the Apache Jena Turtle writer just ignores the base value completely
    // (given to it as the last argument in the next line).
    outputRdfWriter.write("@base <$base> .\n")
    model.write(outputRdfWriter, "TURTLE", base)
}
