// SPDX-FileCopyrightText: 2020 Robin Vobruba <hoijui.quaero@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package de.opensourceecology.tsdc.tools.md2rdf

import org.apache.jena.rdf.model.Model
import java.lang.IllegalStateException

abstract class HierarchyPart(
    private val args: ParsedArgs,
    val origName: String,
    val parent: HierarchyPart?,
    var onlyIfTechnicallyNecessary: Boolean = true,
    val children: MutableList<HierarchyPart> = mutableListOf())
{
    init {
        // only root can have an empty name
        assert(parent == null || origName.isNotBlank())
        parent?.children?.add(this)
    }

    protected fun isRoot() : Boolean {
        return (parent == null)
    }

    protected fun getShallowName() : String {

        return if (isRoot()) {
            "RequirementSpecifier"
        } else {
            MdPart.camelCaseify(origName)
        }
    }

    protected fun constructFullName() : String {

        return if (isRoot()) {
            "RequirementSpecifier"
        } else {
            constructParentFullName() + MdPart.camelCaseify(origName)
        }
    }

    protected fun constructParentFullName() : String {

        if (isRoot()) {
            throw IllegalStateException("Root node has no parent")
        }
        return parent!!.constructFullName()
    }

    // higher level name functions ...

    fun getParentName() : String {

        return if (args.hierarchicalNames) {
            constructParentFullName()
        } else {
            parent!!.getShallowName()
        }
    }

    fun getMyName() : String {

        return if (args.hierarchicalNames) {
            constructFullName()
        } else {
            getShallowName()
        }
    }

    fun isLeaf() : Boolean {
        return children.isEmpty()
    }

    abstract fun addTo(model: Model)
}
