// SPDX-FileCopyrightText: 2020-2024 Robin Vobruba <hoijui.quaero@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package de.opensourceecology.tsdc.tools.md2rdf

/**
 * Raw (pretty raw at least) Markdown structure part.
 */
class MdPart(val name: String, val comment: String?, val onlyIfTechnicallyNecessary: Boolean) {

    override fun toString() : String {
        return name
    }

    companion object {
        private fun camelCaseifyStrings(strs: List<String>) : String {
            return strs.fold("") { acc: String, it: String -> acc + it.replaceFirstChar { it.uppercase() } }
        }
        fun camelCaseify(str: String) : String {
            return camelCaseifyStrings(str.split(" ", "-"))
        }

        fun fromName(name: String) : MdPart {
            var onlyIfTechnicallyNecessary = false
            var comment : String? = null
            var cleanName = name

            if (cleanName.contains("[if technically necessary]")) {
                cleanName = cleanName.replace("[if technically necessary]", "")
                onlyIfTechnicallyNecessary = true
            }

            if (cleanName.contains('(')) {
                val parts1 = cleanName.split('(', limit = 2)
                cleanName = parts1[0].trim()
                comment = parts1[1].replaceAfterLast(')', "").removeSuffix(")").trim()
            }

            if (cleanName.contains('/')) {
                cleanName = cleanName.replace("/", " and ")
            }

            cleanName = camelCaseify(cleanName)

            return MdPart(cleanName, comment, onlyIfTechnicallyNecessary)
        }
    }
}
