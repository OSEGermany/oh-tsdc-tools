// SPDX-FileCopyrightText: 2020 Robin Vobruba <hoijui.quaero@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package de.opensourceecology.tsdc.tools.md2rdf

import de.opensourceecology.tsdc.tools.*
import org.apache.jena.rdf.model.Model

class ClassHP(
    args: ParsedArgs,
    origName: String,
    parent: HierarchyPart?,
    onlyIfTechnicallyNecessary: Boolean = true,
    children: MutableList<HierarchyPart> = mutableListOf())
    : HierarchyPart(args, origName, parent, onlyIfTechnicallyNecessary, children)
{
    override fun addTo(model: Model) {

        val propA = model.createProperty("${IRI_RDF}type")
        val resClass = model.createResource("${IRI_OWL}Class")
        val propIsBelow = model.createProperty("${IRI_TSDC}isBelow")

        val resEntity = model.createResource("${IRI_TSDC_REQ}${getMyName()}")
        val resParentEntity = model.createResource("${IRI_TSDC_REQ}${getParentName()}")

        val entityIsClass = model.createStatement(resEntity, propA, resClass)
        model.add(entityIsClass)
        val entityIsBelowParent = model.createStatement(resEntity, propIsBelow, resParentEntity)
        model.add(entityIsBelowParent)
    }
}