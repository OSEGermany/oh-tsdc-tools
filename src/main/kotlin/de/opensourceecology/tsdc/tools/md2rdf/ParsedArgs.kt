// SPDX-FileCopyrightText: 2020 Robin Vobruba <hoijui.quaero@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package de.opensourceecology.tsdc.tools.md2rdf

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import java.io.File

class ParsedArgs(parser: ArgParser) {

    val inputMd : File by parser.storing(
        "-i", "--md-in",
        help = "path to the input Markdown file") { File(this) }
        .default(File("TsDC-DB.md"))

    val inputRdf by parser.storing(
        "-p", "--rdf-in",
        help = "path to the input RDF, containing already converted parts of the Markdown file") { File(this) }
        .default(File("tsdc-req.ttl"))

    val outputRdf by parser.storing(
        "-o", "--rdf-out",
        help = "Path to the output RDF file, where parts converted from the Markdown DB get written to") { File(this) }
        .default(File("tsdc-req_GENERATED.ttl"))

    private val nonHierarchicalNames by parser.flagging(
        "-a", "--no-hierarchy", "--anarchy",
        help = "Whether to produce flat (non hierarchical) subject names")
    val hierarchicalNames = !nonHierarchicalNames
}
